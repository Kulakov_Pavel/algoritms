package org.tasksolution.strings;

public class SubstringInStringCounter {

    public static void main(String[] args) {
        System.out.println(countSubstringInString("niniin", "ni"));
    }

    private static int countSubstringInString(String str, String substr) {
        if(str.isEmpty() || substr.length() > str.length()) {
            return 0;
        }
        int counter = 0;
        for (int i = 0; i <= str.length() - substr.length(); i++) {
            if(str.substring(i).startsWith(substr)) {
                ++counter;
            }
        }
        return counter;
    }

}
