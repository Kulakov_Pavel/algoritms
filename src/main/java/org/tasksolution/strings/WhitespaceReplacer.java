package org.tasksolution.strings;

public class WhitespaceReplacer {
    public static void main(String[] args) {
        System.out.println(clear("s  t   ri  n  g"));
    }

    private static String clear(String str) {
        return str.replaceAll("\\s", "");
    }
}
