package org.tasksolution.strings;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/*
* Посчитать сколько раз встречаются символы в строке
*/

public class CharCounter {
    public static void main(String[] args) {

        System.out.println(countCharsByMap("Task string example"));
        System.out.println(countCharsByStream("Task string example"));

    }

    private static Map<Character, Integer> countCharsByMap(String value) {
        Map<Character, Integer> map = new HashMap<>();
        value.chars()
                .forEach(c -> map.merge((char) c, 1, Integer::sum));
        return map;
    }

    private static Map<Character, Long> countCharsByStream(String value) {
        return value.chars()
                .mapToObj(c -> (char)c)
                .collect(Collectors.groupingBy(ch -> ch, Collectors.counting()));
    }

}
