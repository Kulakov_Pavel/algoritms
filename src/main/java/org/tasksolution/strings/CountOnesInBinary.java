package org.tasksolution.strings;

/*
*    Задана строка с бинарным числом.
*    Требуется посчитать максимальную сумму подстрок с единицами, если между ними стоит не более одного нуля
*   или просто максимального числа единиц
*   "101101" -> 3
*   "011101101" -> 5
*   "101111101" -> 6
*   "100111110001111101" -> 6
* */
public class CountOnesInBinary {

    public static void main(String[] args) {

        String str = "100111110001111101111000000011";

        System.out.println(ones(str));

    }

    private static int ones(String str) {
        boolean substrings = false;
        int res = 0;
        int left = 0;

        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == '0' && !substrings) {
                continue;
            }
            if(str.charAt(i) == '1') {
                int counter = 1;
                while(i + 1 < str.length() && str.charAt(i + 1) == '1') {
                    ++counter;
                    ++i;
                }
                if(!substrings) {
                    left = counter;
                    substrings = true;
                    res = Math.max(counter, res);
                } else {
                    res = Math.max(left + counter, res);
                    left = counter;
                }
                continue;
            }
            if(str.charAt(i) == '0') {
                int counter = 1;
                while (i + 1 < str.length() && str.charAt(i +1) == '0') {
                    ++i;
                    ++counter;
                }
                if(counter > 1) {
                    if(left > res) {
                        res = left;
                    } else {
                        substrings = false;
                    }
                }
            }
        }
        return res;
    }

}
