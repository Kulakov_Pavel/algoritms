package org.tasksolution.strings;

import java.util.Arrays;

public class Anagram {
    public static void main(String[] args) {

        System.out.println(isAnagram("abcd", "dacb"));

    }

    private static boolean isAnagram(String a, String b) {
        Object[] arrayA = a.chars().mapToObj(ch -> (char) ch)
                .filter(Character::isLetterOrDigit)
                .toArray();
        Object[] arrayB = b.chars().mapToObj(ch -> (char) ch)
                .filter(Character::isLetterOrDigit)
                .toArray();
        Arrays.sort(arrayA);
        Arrays.sort(arrayB);
        return Arrays.equals(arrayA, arrayB);
    }
}
