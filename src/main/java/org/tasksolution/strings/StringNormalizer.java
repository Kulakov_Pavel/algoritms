package org.tasksolution.strings;

/*
* Дана строка с повторяющимися символами. Нужно привести строку к нормальному виду, взяв из подмножества
* повторяющихся символов только первый в том регистре, который есть в строке.
* */

public class StringNormalizer {

    public static void main(String[] args) {
        String str = """
                ЛллллюююююЮююююбБбббббблЛЛлюююЮЮ 
                тТТтттеЕЕЕееебББббяя... ПппппеттТТТрРрраааа 
                тТвввворРррренььььье!!!!        
                """;
        System.out.println(normalizeString(str));
    }

    public static String normalizeString(String str) {
        if (str == null || str.isEmpty())
            return "";
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if ((result.isEmpty()) ||
                    !result.toString().toLowerCase().endsWith(String.valueOf(str.charAt(i)).toLowerCase())) {
                result.append(str.charAt(i));
            }
        }
        return result.toString();
    }
}
