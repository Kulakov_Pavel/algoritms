package org.tasksolution.strings;

/*
* Найти кол-во раз, которое символ встречается в строке
*/

public class CharCounting {
    public static void main(String[] args) {

        char searchCharacter = 'a';
        String src = "abracadabra";

        System.out.printf("""
                Character '%s' duplicates %d times at '%s'
                %n""", searchCharacter, countChar(src, searchCharacter), src);

    }

    private static long countChar(String str, char ch) {
        return str.chars()
                .mapToObj(c -> (char) c)
                .filter(el -> el.equals(ch))
                .count();
    }

}
