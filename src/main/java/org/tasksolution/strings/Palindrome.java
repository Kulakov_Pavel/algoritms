package org.tasksolution.strings;

/*
* Написать функцию, которая будет определять, является ли строка палиндромом.
* Палиндомом является строка, которая как спереди, так и сзади читается одинаково
* Пробелы, знаки препинания и регистр не берётся во внимание
* */

public class Palindrome {
    public static void main(String[] args) {

        System.out.println(isPalindrome("is it   tisi"));

    }

    public static boolean isPalindrome(String str) {
        String forward = str.toLowerCase().codePoints()
                .filter(Character::isLetterOrDigit)
                .collect(StringBuilder::new,
                        StringBuilder::appendCodePoint,
                        StringBuilder::append)
                .toString();
        String backward = new StringBuilder(forward).reverse().toString();
        return forward.equals(backward);
    }

}