package org.tasksolution.strings;

import java.util.Arrays;
import java.util.stream.Collectors;

/*
* Написать функцию, которая будет конкатенировать строки
* */

public class StringConcatinator {
    public static void main(String[] args) {
        System.out.println(concat("some", "string"));
        System.out.println(String.join("-", "some", "string"));
    }

    private static String concat(String ...str) {
        return Arrays.stream(str)
                .collect(Collectors.joining("-"));
    }
}
