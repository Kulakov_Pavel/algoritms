package org.tasksolution.strings;

/*
* Проверить, состоит ли строка из цифр только
* */

public class StringContainsDigitsOnly {
    public static void main(String[] args) {
        System.out.println(stringContainsOnlyDigits("0123456789"));
        System.out.println(stringContainsOnlyDigits("o123456789"));
    }

    private static boolean stringContainsOnlyDigits(String str) {
        return str.chars()
                .mapToObj(ch -> (char) ch)
                .allMatch(Character::isDigit);
    }
}
