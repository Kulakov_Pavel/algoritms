package org.tasksolution.strings;

import java.util.HashMap;
import java.util.Map;

/*
Даны две строки s и t. Определите изоморфны ли они.

Определение:
Две строки s и t называются изоморфными, если можно сделать замену символов в строке s,
чтобы получить строку t, при этом:
1. Все вхождения заменяемого символа должны быть заменены заменяющим символом
2. Никакие два разных символа не могут быть заменены на один и тот же символ
3. Символ может быть заменен сам на себя


Примеры:
s = "egg", t = "add" -> True
s = "egg", t = "adе" -> False
s = "foo", t = "bar" -> False
s = "paper", t = "title" -> True
s = "er", t = "le" -> True
s = "aavv", t = "bcdd" -> false
s = "acvv", t = "bbdd" -> false
*/

public class Isomorphic {
    public static void main(String[] args) {
        System.out.println(isIsomorphic("egg", "add"));
        System.out.println(isIsomorphic("egg", "adе"));
        System.out.println(isIsomorphic("foo", "bar"));
        System.out.println(isIsomorphic("paper", "title"));
        System.out.println(isIsomorphic("er", "le"));
        System.out.println(isIsomorphic("aavv", "bcdd"));
        System.out.println(isIsomorphic("acvv", "bbdd"));
    }

    public static boolean isIsomorphic(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        }
        Map<Character, Character> map = new HashMap<>();
        Map<Character, Character> reverseMap = new HashMap<>();

        for (int i = 0; i < a.length(); ++i) {
            Character charA = a.charAt(i);
            Character charB = b.charAt(i);
            if (map.get(charA) == null) {
                if (reverseMap.get(charB) != null &&
                        reverseMap.get(charB) != charA) {
                    return false;
                }
                map.put(charA, charB);
                reverseMap.put(charB, charA);
            } else {
                if (map.get(charA) == charB) {
                    continue;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

}
