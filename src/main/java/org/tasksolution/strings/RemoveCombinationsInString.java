package org.tasksolution.strings;

/*
* Даётся строка и массив строковых комбинаций. Требуется удалить из строки все комбинации.
* */

public class RemoveCombinationsInString {

    public static void main(String[] args) {

        System.out.println(removeChars("amamaaamamamagvamgmamamamamgmamgmamgamgagma", "am", "g"));

    }

    static String removeChars(String str, String ... combinations) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < combinations.length; j++) {
                String comb = combinations[j];
                if(i + comb.length() <= length && str.substring(i).startsWith(comb)) {
                    str = str.substring(0, i) + str.substring(i).replace(combinations[j], "");
                    if(i > 0) {
                        i = i == 1? 0: i-2;
                    }
                    length = str.length();
                }
            }
        }
        return str;
    }

}
