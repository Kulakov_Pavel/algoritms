package org.tasksolution.strings;

import java.util.Arrays;
import java.util.List;

/*
*  Дана строка в виде URI пути.
*  Требуется вернуть чистый прямой URI без лишних входов/выходов в папки
* */

public class UriPathCleaner {

    public static void main(String[] args) throws InterruptedException {

        String str = "/foo/../test/../test/../foo//bar/./baz";
        System.out.println(getClearPath(str));
    }

    static String getClearPath(String str) {
        List<String> strings = Arrays.stream(str.split("/"))
                .filter(el -> !el.equals("/") && !el.equals(".") && !el.isEmpty())
                .toList();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strings.size() -1; i++) {
            if(strings.get(i + 1).equals("..")) {
                ++i;
                continue;
            } else {
                sb.append(strings.get(i)).append("/");
                if(i == strings.size() - 2 && !strings.get(i + 1).equals("..")) {
                    sb.append(strings.get(i+1));
                }
            }
        }
        return sb.toString();
    }

}
