package org.tasksolution.strings;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/*
* Найти первый неповторяющийся символ в строке
* */

public class FirstUniqueChar {
    public static void main(String[] args) {

        System.out.println(findFirstUniqueChar("string for searching").orElse(null));

    }

    private static Optional<Character> findFirstUniqueChar(String value) {
        LinkedHashMap<Character, Long> map = value.chars()
                .mapToObj(ch -> (char)ch)
                .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()));
        return map.entrySet().stream()
                .filter(e -> e.getValue() == 1)
                .map(Map.Entry::getKey)
                .findFirst();
    }

}
