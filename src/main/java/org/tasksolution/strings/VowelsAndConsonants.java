package org.tasksolution.strings;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
 * Написать ф=ию, которая посчитает кол-во чётных и нечётных символов в строке
 * */

public class VowelsAndConsonants {
    public static void main(String[] args) {

        System.out.println(countVowelsAndConsonants("abcde eb"));

    }

    private static Map<String, List<Character>> countVowelsAndConsonants(String str) {
        List<Character> vowels = List.of('a', 'e', 'i', 'o', 'u');
        Map<Boolean, List<Character>> aggregation =
                str.toLowerCase()
                        .replaceAll("\\s", "")
                        .chars()
                        .mapToObj(ch -> (char) ch)
                        .collect(Collectors.partitioningBy(vowels::contains));
        return Map.of("Vowels", aggregation.get(true),
                "Consonants", aggregation.get(false));

    }

}
