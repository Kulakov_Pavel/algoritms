package org.tasksolution.concurrency;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static java.util.concurrent.TimeUnit.SECONDS;

public class SupplierConsumer {

    private static final Object lock = SupplierConsumer.class;
    private static final Queue<String> queue = new LinkedList<>();
    private static final Executor executor = Executors.newFixedThreadPool(16);
    private static volatile int counter;
    private static final Random random = new Random();
    private static final AtomicInteger consumers = new AtomicInteger();
    private static final AtomicInteger suppliers = new AtomicInteger();


    public static void main(String[] args) {
        IntStream.range(5, random.nextInt(20))
                .mapToObj(i -> {
                    if (random.nextInt(10) % 2 == 0) {
                        suppliers.incrementAndGet();
                        return new Thread(SupplierConsumer::supplier);
                    } else {
                        consumers.incrementAndGet();
                        return new Thread(SupplierConsumer::consumer);
                    }
                })
                .forEach(executor::execute);

        System.out.printf("""
                        =================
                        | Suppliers: %s |
                        | Consumers: %s |
                        =================
                        
                        """,
                suppliers.get(), consumers.get());
    }

    static void supplier() {
        for (; ; ) {
            synchronized (lock) {
                while (queue.size() >= 10) {
                    await();
                }
                asleep();
                System.out.println(Thread.currentThread().getName() + " added task #" + ++counter);
                queue.offer(getCurrentTask(counter));
                System.out.println("Queue size = " + queue.size() + "\n");
                lock.notifyAll();
            }
            asleep();
        }
    }

    static void consumer() {
        for (; ; ) {
            synchronized (lock) {
                while (queue.isEmpty()) {
                    await();
                }
                asleep();
                System.out.println(Thread.currentThread().getName() + " removed task " + queue.poll());
                System.out.println("Queue size = " + queue.size() + "\n");
                lock.notifyAll();
            }
            asleep();
        }
    }

    private static String getCurrentTask(int task) {
        return "Task #" + task;
    }

    private static void await() {
        try {
            lock.wait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private static void asleep() {
        try {
            SECONDS.sleep(random.nextInt(3));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
