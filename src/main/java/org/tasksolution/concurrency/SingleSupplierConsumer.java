package org.tasksolution.concurrency;

import java.util.LinkedList;
import java.util.Queue;

import static java.util.concurrent.TimeUnit.SECONDS;

public class SingleSupplierConsumer {

    private static final Queue<String> queue = new LinkedList<>();
    private static volatile int counter = 0;
    private static final Object lock = SingleSupplierConsumer.class;

    public static void main(String[] args) {
        new Thread(new Producer()).start();
        new Thread(new Consumer()).start();
    }

    private static class Producer implements Runnable {
        @Override
        public void run() {

            for (; ; ) {
                synchronized (lock) {
                    var message = "message#" + counter++;
                    System.out.println("Producer: " + message);
                    queue.offer(message);
                    lock.notify();
                    await();
                    asleep(2);
                }
            }
        }
    }

    private static class Consumer implements Runnable {
        @Override
        public void run() {
            for (; ; ) {
                synchronized (lock) {
                    while (queue.isEmpty()) {
                        await();
                    }
                    System.out.println("Consumer: " + queue.poll());
                    lock.notify();
                }
            }
        }
    }

    private static void asleep(int sec) {
        try {
            SECONDS.sleep(sec);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private static void await() {
        try {
            lock.wait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
