package org.tasksolution.concurrency;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PingPongWithReentrantLock {
    private static boolean started;
    private static final Lock lock = new ReentrantLock();
    private static final Condition condition = lock.newCondition();

    public static void main(String[] args) {

        Thread ping = new Thread(PingPongWithReentrantLock::ping);
        Thread pong = new Thread(PingPongWithReentrantLock::pong);
        pong.start();
        ping.start();

    }

    public static void ping() {
        try {
            lock.lock();
            if (!started) {
                started = true;
                System.out.println("Ping's started the game...");
            }
            for (; ; ) {
                sleep(1);
                System.out.println("ping");
                condition.signal();
                await();
            }
        } finally {
            lock.unlock();
        }
    }

    public static void pong() {
        try {
            lock.lock();
            if (started) {
                for (; ; ) {
                    sleep(1);
                    System.out.println("pong");
                    condition.signal();
                    await();
                }
            } else {
                System.out.println("Pong rolls ball to Ping");
                await();
                pong();
            }
        } finally {
            lock.unlock();
        }
    }

    public static void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private static void await() {
        try {
            condition.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
