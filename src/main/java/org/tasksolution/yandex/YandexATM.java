package org.tasksolution.yandex;

import java.math.BigInteger;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 Банкомат.
 Инициализируется набором купюр и умеет выдавать купюры для заданной суммы, либо отвечать отказом.
 При выдаче купюры списываются с баланса банкомата.
 Допустимые номиналы: 50₽, 100₽, 500₽, 1000₽, 5000₽.
 */

public class YandexATM {

    private final Map<Integer, Integer> atm;
    private final List<Integer> banknotes;
    private BigInteger balance;
    private final int minBanknote;
    public YandexATM(Map<Integer, Integer> atm) {
        this.atm = new HashMap<>(atm);
        balance = BigInteger.ZERO;
        atm.forEach((key, value) -> balance = balance.add(
                BigInteger.valueOf((long) key * value)));
        minBanknote = atm.keySet().stream()
                .min(Integer::compareTo).orElse(50);
        banknotes = atm.keySet().stream()
                .sorted(Integer::compareTo)
                .sorted(Comparator.reverseOrder())
                .toList();
    }

    public Map<Integer, Integer> getAmount(Integer amount) {
        Map<Integer, Integer> result = new HashMap<>();
//        1.  Проверка на наличие  суммы в банкомате, иначе кидаем custom Exception
        if(amount > balance.intValue()) {
            System.out.println("В автомате недостаточно денег");
            return result;

//        2.  Проверка суммы на кратность минимальной купюре, иначе кидаем custom Exception
        } else if (amount % minBanknote != 0) {
            System.out.println("В автомате нет соответствующих купюр");
            return result;
        }

//        3.  Выдача денег
        for (int banknote : banknotes) {
            if (amount >= banknote && atm.get(banknote) != 0) {
                int currentBanknoteAmount = amount / banknote;
                if (currentBanknoteAmount <= atm.get(banknote)) {
                    result.put(banknote, currentBanknoteAmount);
                    atm.put(banknote, atm.get(banknote) - currentBanknoteAmount);
                    int cashWithdrow = banknote * currentBanknoteAmount;
                    amount -= cashWithdrow;
                    balance = balance.add(BigInteger.valueOf(cashWithdrow).negate());
                } else {
                    result.put(banknote, atm.get(banknote));
                    atm.put(banknote, 0);
                    amount -= result.get(banknote) * banknote;
                    balance = balance.add(BigInteger.valueOf((long) result.get(banknote) * banknote).negate());
                }
            }
        }

        return result;
    }

}