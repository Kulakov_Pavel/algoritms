package org.tasksolution;

import org.tasksolution.yandex.YandexATM;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        Map<String, Integer> copy = Map.of("one", 12,
                "three", 33);

        map.computeIfAbsent("one", k -> 1 * 1);
        map.computeIfAbsent("one", k -> 2);

        map.computeIfPresent("one", (k, v) -> 11);
        map.computeIfPresent("two", (k, v) -> 2);

        map.compute("three", (k,v) -> 3);
        map.compute("three", (k,v) -> 33);

        map.merge("one", 1, (o,n) -> o + n);

        System.out.println(map.equals(copy)); // true

        YandexATM atm = new YandexATM(Map.of(500, 1));
        atm.getAmount(500);


    }
}





















