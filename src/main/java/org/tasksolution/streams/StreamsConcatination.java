package org.tasksolution.streams;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamsConcatination {

    public static void main(String[] args) {

    }

    public static List<String> getElementsFromConcatinatedLists() {
        List<String> strings1 = List.of("one", "two", "three", "four", "five");
        List<String> strings2 = List.of("six", "seven", "eight", "nine", "ten");

        return java.util.stream.Stream.of(strings1.stream(), strings2.stream())
                .flatMap(Function.identity())
                .collect(Collectors.toList());
    }

}
