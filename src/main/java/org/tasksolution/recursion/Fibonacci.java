package org.tasksolution.recursion;

public class Fibonacci {
    public static void main(String[] args) {
        //0 1 1 2 3 5 8 13 21
        System.out.println(fibonacci(5));
    }

    private static int fibonacci(int i) {
        return i == 0 || i == 1 ? 1 : fibonacci(i - 1) + fibonacci(i - 2);
    }
}
