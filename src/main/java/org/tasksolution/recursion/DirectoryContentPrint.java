package org.tasksolution.recursion;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DirectoryContentPrint {

    public static void main(String[] args) {
        walk(Paths.get("/home/pavel/Desktop/"), "");
    }

    private static void walk(Path path, String offset) {
        System.out.print(offset);
        if (path.toFile().isFile()) {
            System.out.println(path.getFileName());
        } else {
            offset += "    ";
            System.out.println(path.getFileName());
            try (var list = Files.list(path)) {
                String finalOffset = offset;
                list.forEach(p -> walk(p, finalOffset));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
