package org.tasksolution.recursion;

public class Factorial {
    public static void main(String[] args) {
        System.out.println(factorial(5));
    }

    private static int factorial(int i) {
        return i ==0 || i == 1 ? 1: i * factorial(i -1);
    }
}
