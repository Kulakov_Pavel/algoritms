package org.tasksolution.list;

public class DeleteNthElementFromTheEnd {
    public static void main(String[] args) {

        ListNode head = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(head);
        head = removeNthFromEnd(head, 2);
        System.out.println(head);

    }

    private static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode start, fast, slow;
        start = fast = slow = new ListNode(0, head);

        for (int i = 0; i < n; i++) {
            fast = fast.next;
        }
        while (fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }
        slow.next = slow.next.next;
        return start.next;
    }

    static class ListNode {
        int val;
        ListNode next;

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public String toString() {
            return next == null ?
                    "" + val :
                    val + " -> " + next.toString();
        }
    }
}
