package org.tasksolution.numbers;

import java.util.stream.IntStream;

public class Numbers {

    public static void main(String[] args) {

    }

    public static boolean isPrime(int num) {
        int border = (int)Math.sqrt(num);
        return num == 2 || num > 1 && IntStream.rangeClosed(2, border)
                .noneMatch(v -> num / v == 0);
    }

}
