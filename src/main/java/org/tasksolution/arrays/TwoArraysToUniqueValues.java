package org.tasksolution.arrays;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TwoArraysToUniqueValues {

    public static void main(String[] args) throws InterruptedException {

        int[] a = new int[]{1, 2, 3, 4, 5};
        int[] b = new int[]{1, 2, 4, 5, 6};

        List<Integer> unique = Stream.of(Arrays.stream(a), Arrays.stream(b))
                .flatMapToInt(Function.identity())
                .boxed()
                .sorted()
                .distinct()
                .collect(Collectors.toList());
        System.out.println(unique);

    }

       


}
