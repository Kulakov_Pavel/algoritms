package org.tasksolution.arrays;

import java.util.Arrays;

public class ArraysHaveSameContent {
    public static void main(String[] args) {
        System.out.println(arraysHaveSameContent(new Integer[]{1, 3, 2}, new Integer[]{3, 1, 2}));
        System.out.println(arraysHaveSameContent(new Integer[]{1, 3, 2}, new String[]{"b"}));
    }

    private static <T> boolean arraysHaveSameContent(T[] a, T[] b) {
        if (a == b) {
            return true;
        }
        Arrays.sort(a);
        Arrays.sort(b);
        return Arrays.equals(a, b);
    }
}
