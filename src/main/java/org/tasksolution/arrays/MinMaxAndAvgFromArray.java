package org.tasksolution.arrays;

import java.util.Arrays;

public class MinMaxAndAvgFromArray {
    public static void main(String[] args) {
        System.out.println(getStatisticFromArray(new Double[]{}));
    }

    private static <T extends Number & Comparable<T>> SummaryStatistic<T> getStatisticFromArray(T[] arr) {
        T min = Arrays.stream(arr)
                .min(Comparable::compareTo).orElse(null);
        T max = Arrays.stream(arr)
                .max(Comparable::compareTo).orElse(null);
        double total = Arrays.stream(arr)
                .mapToDouble(T::doubleValue)
                .reduce(0.0, Double::sum);
        double avg = arr.length == 0 ? total : total / arr.length;

        return new SummaryStatistic<T>(min, avg, max);
    }

    private static class SummaryStatistic<T> {
        T min;
        Double avg;
        T max;

        SummaryStatistic(T min, Double avg, T max) {
            this.min = min;
            this.avg = Double.valueOf(String.format("%.2f", avg));
            this.max = max;
        }

        @Override
        public String toString() {
            return "SummaryStatistic{" +
                    "min=" + min +
                    ", avg=" + avg +
                    ", max=" + max +
                    '}';
        }
    }
}
