package org.tasksolution.arrays;

import java.util.Arrays;

public class ArrayIncrease {
    public static void main(String[] args) {
        Integer[] arr = {1, 2, 3};
        addElement(arr, 4);
        System.out.println(Arrays.asList(addElement(arr, 4)));
    }

    private static <T> T[] addElement(T[] arr, T el) {
        arr = Arrays.copyOf(arr, arr.length + 1);
        arr[arr.length -1] = el;
        return arr;
    }
}
