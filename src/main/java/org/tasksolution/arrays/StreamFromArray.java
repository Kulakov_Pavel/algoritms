package org.tasksolution.arrays;

import java.util.Arrays;
import java.util.stream.Stream;

public class StreamFromArray {
    public static void main(String[] args) {
        streamFromArray(new Integer[]{1, 2, 3}).forEach(System.out::println);
    }

    private static <T> Stream<T> streamFromArray(T[] arr) {
        return Arrays.stream(arr);
    }
}
