package org.tasksolution.arrays;

public class FindElementInArray {

    public static void main(String[] args) {

        System.out.println(findIndexOfElement(new Integer[]{1, 3, 5}, 4));

    }

    private static <T> int findIndexOfElement (T[] arr, T el) {
        for (int i = 0; i < arr.length; i++) {
            if(arr[i].equals(el)) {
                return i;
            }
        }
        return -1;
    }

}
