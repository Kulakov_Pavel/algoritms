package org.tasksolution.arrays;

import java.util.Arrays;
import java.util.Comparator;

public class ArraySort {
    public static void main(String[] args) {

        System.out.println(Arrays.asList(sort(new String[]{"one", "two", "three"})));
        System.out.println(Arrays.asList(sortByStream(new String[]{"one", "two", "three"})));
        System.out.println(Arrays.asList(sort(new String[]{"one", "two", "three"}, String::compareTo)));
        System.out.println(Arrays.asList(sort(new String[]{"one", "two", "three"}, (a, b) -> a.compareTo(b))));

    }

    private static <T> T[] sort(T[] arr) {
        Arrays.sort(arr);
        return arr;
    }

    private static <T> T[] sort(T[] arr, Comparator<T> comparator) {
        Arrays.sort(arr, comparator);
        return arr;
    }

    @SuppressWarnings("unchecked")
    private static <T> T[] sortByStream(T[] arr) {
        Object[] objects = Arrays.stream(arr)
                .sorted()
                .toArray();
        return (T[])Arrays.copyOf(objects, objects.length, arr.getClass());
    }

}
