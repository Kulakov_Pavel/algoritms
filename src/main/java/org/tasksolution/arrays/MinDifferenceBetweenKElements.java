package org.tasksolution.arrays;

import java.util.Arrays;

/*
* Дан массив чисел и число К.
* Нужно найти связку К чисел, разница между наименьшим и наибольшим из которой будет наименьшей во всём массиве
* */
public class MinDifferenceBetweenKElements {

    public static void main(String[] args) {

        int[] data = {-5, -3, 0, 1, 2, 4};
        System.out.println(getData(data, 3));

    }

    public static int getData(int[] data, int k) {
        Arrays.sort(data);
        int res = Integer.MAX_VALUE;
        --k;
        for (int i = 0; i < data.length - k; i++) {
            int dif = data[i + k] - data[i];
            if(dif < res) {
                res = dif;
            }
        }
        return res;
    }

}
