package org.tasksolution.arrays;

import java.util.Arrays;
import java.util.stream.IntStream;

/*
*  Есть массив чисел. Нужно вернуть отсортированный массив этих чисел в квадрате
* */

public class ArrayOfIntegersToArrayOfSquareIntegers {

    public static void main(String[] args) {

        int[] data = {-5, -3, 0, 1, 2, 4};
        System.out.println(Arrays.toString(sortSquares(data)));

    }

    public static int[] sortSquares(int[] data) {
        return IntStream.of(data)
                .map(e -> e * e)
                .sorted()
                .toArray();
    }

}
