package org.tasksolution.arrays;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/*
*  Найти первый уникальный элемент в массиве
* */

public class FirstUniqueElementInArrayIntegers {
    public static void main(String[] args) {
        Integer[] arr = {9, 4, 9, 9, -1, 0, 6, 7, 4, 5, -1, 0};
        System.out.println(findFirstUniqueValue(arr));
    }

    private static <T extends Serializable> T findFirstUniqueValue(T[] arr) {
        Map<T, Integer> map = new LinkedHashMap<>();
        Arrays.stream(arr).forEach(el -> map.merge(el, 1, Integer::sum));
        Optional<T> result = map.entrySet().stream()
                .filter(es -> es.getValue() == 1)
                .map(Map.Entry::getKey)
                .findFirst();
        return result.orElse(null);
    }
}
