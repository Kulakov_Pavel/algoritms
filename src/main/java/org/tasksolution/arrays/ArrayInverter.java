package org.tasksolution.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArrayInverter {
    public static void main(String[] args) {
        var arr = new Integer[]{1, 2, 3};
        System.out.println(Arrays.asList(arr));
        invertArray(arr);
        System.out.println(Arrays.asList(arr));
    }

    private static <T> List<T> invertArrayByCollection(T[] arr) {
        Collections.reverse(Arrays.asList(arr));
        return new ArrayList<T>(Arrays.asList(arr));
    }

    private static <T> void invertArray(T[] arr) {
        T tmp;
        for (int i = 0; i < arr.length / 2; i++) {
            tmp = arr[i];
            arr[i] = arr[arr.length - (i+1)];
            arr[arr.length - (i+1)] = tmp;
        }
    }

}
