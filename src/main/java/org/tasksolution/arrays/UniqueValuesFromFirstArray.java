package org.tasksolution.arrays;

// Даны два отсортированных массива.
// Вывести все элементы первого массива, которые не встречаются во втором.

// Пример
// filter([1, 2, 3], [2, 3, 4]) => [1]

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UniqueValuesFromFirstArray {
    public static void main(String[] args) {

        System.out.println(binarySearchUniqueValuesFromFirstArray(new int[]{1, 2, 3}, new int[]{2, 3, 4}));
        System.out.println(streamSearchUniqueValuesFromFirstArray(new Integer[]{1, 2, 3}, new Integer[]{2, 3, 4}));

    }

    private static List<Integer> binarySearchUniqueValuesFromFirstArray(int[] a, int[] b) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < a.length; i++) {
            if(Arrays.binarySearch(b, a[i]) < 0) {
                result.add(a[i]);
            }
        }
        return result;
    }

    private static List<Integer> streamSearchUniqueValuesFromFirstArray(Integer[] a, Integer[] b) {
        List<Integer> listB = Arrays.asList(b);
        return Arrays.stream(a)
                .filter(v -> !listB.contains(v))
                .toList();
    }
}
